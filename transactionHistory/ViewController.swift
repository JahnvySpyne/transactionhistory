//
//  ViewController.swift
//  transactionHistory
//
//  Created by Jahnvy Pandey on 23/03/23.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var sliideButton: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "TableViewCell")
        tableView.register(UINib(nibName: "TransactionHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: TransactionHeaderFooterView.reuseIdentifier)
        
        bottomView.layer.borderWidth = 1
        bottomView.layer.borderColor = UIColor.lightGray.cgColor
        bottomView.layer.cornerRadius = 10
        topView.layer.cornerRadius = 10
        tableView.showsVerticalScrollIndicator = false
        sliideButton.layer.borderWidth = 1
        sliideButton.layer.borderColor = UIColor.lightGray.cgColor
        sliideButton.layer.cornerRadius = 10

    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if let customCell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell" , for: indexPath) as? TableViewCell {
            return customCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TransactionHeaderFooterView") as! TransactionHeaderFooterView
        headerView.headerLabel.layer.borderWidth = 1
        headerView.headerLabel.layer.borderColor = UIColor.lightGray.cgColor
        headerView.headerLabel.layer.cornerRadius = 10
        
        return headerView
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let cell = UITableViewCell()
//       if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TransactionDateHeader") as! TransactionDateHeader
//    }
}

